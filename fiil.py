#!/usr/bin/env python
# -*- coding: utf-8 -*-
import wx, sys, os
from kaynak import *
from heceleme import *
from synonymus import *
from meaning import *
class LoginFrame(wx.Frame):
    def __init__(self, parent, id, title, size, style = wx.DEFAULT_FRAME_STYLE ):
        wx.Frame.__init__(self, parent, id, title, size=size, style=style)
        self.panel = MainPanel(self) 
        #title_text = wx.StaticText(self.panel, label="NECMETTİN ERBAKAN UNIVERSTY\n     Secure Connection Service ", pos=(110, 20))
        spellout_text = wx.StaticText(self.panel, label="Spell Out:", pos=(105, 85))
        verb_text = wx.StaticText(self.panel, label="Verb      :", pos=(105, 160))
        synonyms_text=wx.StaticText(self.panel, label="Synonymus:", pos=(105, 250))
        meaning_text=wx.StaticText(self.panel,label="Meaning   :",pos=(105,330))

        self.spellout_result = wx.StaticText(self.panel, label="SPELL RESULT?", pos=(380, 80),style=(wx.TE_READONLY))
        self.verb_result=  wx.StaticText(self.panel, label="VERB RESULT?", pos=(380, 160),style=(wx.TE_READONLY))
        self.synonyms_result=wx.StaticText(self.panel, label="SYNONYMS RESULT?", pos=(380, 250),style=(wx.TE_READONLY))
        self.meaning_result=wx.StaticText(self.panel,label="MEANİNG RESULT?",pos=(380,330),style=(wx.TE_READONLY))
        font = wx.Font(10, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, True)
        #font2 = wx.Font(9, wx.NORMAL, wx.LIGHT, wx.BOLD)
        
        self.spellout=wx.TextCtrl(self.panel, 1,size=(170,30),style=(wx.TE_LEFT),pos=(200,80))
        self.verb=wx.TextCtrl(self.panel, 1,size=(170,30),style=(wx.TE_LEFT),pos=(200,160))
        self.synonyms=wx.TextCtrl(self.panel, 1,size=(170,30),style=(wx.TE_LEFT),pos=(200,250))
        self.meaning=wx.TextCtrl(self.panel,1,size=(170,30),style=(wx.TE_LEFT),pos=(200,330))
        #self.logo = wx.StaticBitmap(self.panel,size=(10,10),pos=(30,10))
        #self.logo.SetBitmap(wx.Bitmap('image/logo1.png'))
        
        self.s_spel_out= wx.Button(self.panel,1, label="SHOW",pos=(200,125),size=(70,30))
        self.s_verb= wx.Button(self.panel,2, label="SHOW",pos=(200,200),size=(70,30))
        self.s_synonyms=wx.Button(self.panel,3, label="SHOW",pos=(200,290),size=(70,30))
        self.s_meaning=wx.Button(self.panel,4,label="SHOW",pos=(200,370),size=(70,30))

        
        #title_text.SetFont(font)
        spellout_text.SetFont(font)
        verb_text.SetFont(font)
        synonyms_text.SetFont(font)
        meaning_text.SetFont(font)
        
        self.Bind(wx.EVT_BUTTON, self.OnClick,self.s_spel_out)
        self.Bind(wx.EVT_BUTTON, self.OnClick2,self.s_verb)
        self.Bind(wx.EVT_BUTTON, self.OnClick3,self.s_synonyms)
        self.Bind(wx.EVT_BUTTON, self.OnClick4,self.s_meaning)
        
   

    def OnClick(self,event):
        print "onclick calıstı"
        data=self.spellout.GetValue()
    	c=ana(data)
        c=c.split(" ")
        self.spellout_result.SetLabel(c[0]+","+c[1]+" hece")

    def OnClick2(self,event):
        print "onclick2 calıstı"
        data=self.verb.GetValue()
        c=ana_fonk(data)
        self.verb_result.SetLabel(c)
    def OnClick3(self,event):
        print "onclick3 calıstı"
        data=self.synonyms.GetValue()
        c=main_fonk(data)
        self.synonyms_result.SetLabel(c)
    def OnClick4(self,event):
        print ("onclick4 calıstı")
        data=self.meaning.GetValue()
        c=kelime_al(data)
        self.meaning_result.SetLabel(c)


class MyApp(wx.App):
    def OnInit(self):
        frame = LoginFrame(None, 1, "WORDS-API (TURKISH)",wx.Size(1366,768))
        frame.Show()
        return True
class MainPanel(wx.Panel):
    
    def __init__(self, parent):
        
        wx.Panel.__init__(self, parent=parent)
        self.SetBackgroundStyle(wx.BG_STYLE_CUSTOM)
        self.frame = parent

        sizer = wx.BoxSizer(wx.VERTICAL)
        hSizer = wx.BoxSizer(wx.HORIZONTAL)

        self.SetSizer(hSizer)
        self.Bind(wx.EVT_ERASE_BACKGROUND, self.arkaplan)

    def arkaplan(self, evt):
       
        dc = evt.GetDC()

        if not dc:
            dc = wx.ClientDC(self)
            rect = self.GetUpdateRegion().GetBox()
            dc.SetClippingRect(rect)
        dc.Clear()
        bmp = wx.Bitmap("nlp.jpg")
        dc.DrawBitmap(bmp, 0, 0)
def main(argv=None):
   
    app = MyApp(0)
    app.MainLoop()
if __name__ == '__main__':
    main()
